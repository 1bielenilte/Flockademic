import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ArticleList } from '../../src/components/articleList/component';

const mockProps = {
  articles: [
    {
      // tslint:disable-next-line:max-line-length
      description: 'In this article we discuss different approaches to calculating the optimal routes for intergalactic bypasses.',
      identifier: 'arbitrary-id',
      name: 'Route optimisations for intergalactic bypasses',
    },
    {
      // tslint:disable-next-line:max-line-length
      description: 'We discuss a novel approach to relocating dwellers of homes located in the path of future bypasses.',
      identifier: 'arbitrary-id',
      name: 'On the relocation of human debris',
    },
  ],
};

it('should not render anything when there are no articles', () => {
  const mockArticles = [];

  const overview = shallow(<ArticleList {...mockProps} articles={mockArticles} />);

  expect(overview).toBeEmptyRender();
});

it('should display article names and descriptions', () => {
  const mockArticles = [
    { identifier: 'some_article_id', name: 'Some article', description: 'Some abstract' },
  ];

  const overview = shallow(<ArticleList {...mockProps} articles={mockArticles} />);

  expect(overview.find('[itemType="https://schema.org/ScholarlyArticle"]')).toExist();
  expect(overview.find('[itemType="https://schema.org/ScholarlyArticle"] [itemProp="name"] Link').prop('to'))
    .toMatch('/article/some_article_id');
  expect(overview.find('[itemType="https://schema.org/ScholarlyArticle"] [itemProp="description"]').text())
    .toMatch('Some abstract');
});

it('should link to the ORCID work overview page when the article is an ORCID work', () => {
  const mockArticles = [
    { sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
  ];

  const overview = shallow(<ArticleList {...mockProps} articles={mockArticles} />);

  expect(overview.find('Link[to="/article/0000-0002-4013-9889:1337"]')).toExist();
});

it('should not link to the ORCID work overview page when the work has been linked to an existing article', () => {
  const mockArticles = [
    { identifier: 'some-id', sameAs: 'https://orcid.org/0000-0002-4013-9889/work/1337' },
  ];

  const overview = shallow(<ArticleList {...mockProps} articles={mockArticles} />);

  expect(overview.find('Link[to="/article/0000-0002-4013-9889:1337"]')).not.toExist();
  expect(overview.find('Link[to="/article/some-id"]')).toExist();
});

it('should add metadata when it is part of a journal', () => {
  const mockArticles = [
    {
      identifier: 'arbitrary_article_id',
      isPartOf: { identifier: 'arbitrary-journal-id', url: '/journal/arbitrary-journal-id' },
    },
  ];

  const overview = shallow(<ArticleList {...mockProps} articles={mockArticles} />);

  expect(overview.find('[itemProp="hasPart"]')).toExist();
});

it('should not encourage adding new articles when no submission link was provided by the parent component', () => {
  const overview = shallow(<ArticleList {...mockProps} submissionLink={undefined} />);

  expect(overview.find('[data-test-id="ghostItem"]')).not.toExist();
});

it('should encourage adding new articles when a submission link was provided by the parent component', () => {
  const overview = shallow(<ArticleList {...mockProps} submissionLink="/submission/link" />);

  expect(overview.find('[data-test-id="ghostItem"]')).toExist();
  expect(overview.find('[data-test-id="ghostItem"] Link').prop('to')).toBe('/submission/link');
});
