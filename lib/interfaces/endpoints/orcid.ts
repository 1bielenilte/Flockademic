export interface GetOrcidResponse {
  'activities-summary': {
    educations: {
      'education-summary': Array<{
        'role-title': string;
        organization: {
          name: string;
        };
        'start-date': {
          year: { value: string; };
          month: { value: string; };
          day: { value: string; };
        };
        'end-date': null | {
          year: { value: string; };
          month: { value: string; };
          day: { value: string; };
        };
      }>;
    };
    employments: {
      'employment-summary': Array<{
        'role-title': string;
        organization: {
          name: string;
        };
        'start-date': {
          year: { value: string; };
          month: { value: string; };
          day: { value: string; };
        };
        'end-date': null | {
          year: { value: string; };
          month: { value: string; };
          day: { value: string; };
        };
      }>;
    };
    works: {
      group: Array<{
        'work-summary': [ OrcidWorkSummary ];
      }>;
    };
  };
  person: {
    biography: null | {
      content: null | string;
    },
    name: {
      'family-name': null | {
        value: string;
      };
      'given-names': {
        value: string;
      };
      path: string;
    };
    keywords: {
      keyword: Array<{
        content: string;
      }>;
    };
    'researcher-urls': {
      'researcher-url': Array<{
        'url-name': string;
        url: { value: string; };
      }>;
    };
  };
}

export interface GetOrcidSearchResponse {
  result: Array<{
    'orcid-identifier': {
      uri: string;
      path: string;
      host: string;
    };
  }>;
}

export type GetOrcidProfileSearchResponse = GetOrcidResponse[];

export interface GetOrcidWorkResponse extends OrcidWorkSummary {
  'journal-title': null | { value: string; };
  'short-description': null | string;
  url: null | { value: string };
}

interface OrcidWorkSummary {
  title: {
    title: { value: string; };
    subtitle: null | { value: string; };
  };
  'external-ids': null | {
    'external-id': Array<{
      'external-id-type': 'doi' | string;
      'external-id-value': string;
      'external-id-url': null | { value: string };
    }>;
  };
  'publication-date': null | {
    year: { value: string; };
    month: { value: string; };
    day: { value: string; };
  };
  'put-code': number;
}
